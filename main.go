package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/mironet/server"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

const (
	envVarPrefix = "CLUSTER_HEALTH_CHECKER_"
)

func main() {
	var address string
	var debug bool
	var hcPath string
	var cacheValidity time.Duration
	app := &cli.App{}
	app.Name = "cluster-health-checker"
	app.Usage = "Health checker for resources on k8s clusters."
	app.Commands = []*cli.Command{
		{
			Name:  "run",
			Usage: "Run the health checker server.",
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name: "listen", Aliases: []string{"l"}, Value: ":8080", Usage: "Defines where the health check server is listening on.",
					Destination: &address, EnvVars: []string{envVarPrefix + "LISTEN"},
				},
				&cli.BoolFlag{
					Name: "debug", Aliases: nil, Value: false, Usage: "Use this flag to log on debug level.",
					Destination: &debug, EnvVars: []string{envVarPrefix + "DEBUG"},
				},
				&cli.StringFlag{
					Name: "path", Aliases: []string{"p"}, Value: "/healthcheck", Usage: "Defines where the path where the healthcheck is performend.",
					Destination: &hcPath, EnvVars: []string{envVarPrefix + "PATH"},
				},
				&cli.DurationFlag{
					Name: "cache-validity", Aliases: []string{"v"}, Value: 5 * time.Second, Usage: "Defines for how long the healthiness of a service is kept in cache. Before it is actually checked again.",
					Destination: &cacheValidity, EnvVars: []string{envVarPrefix + "CACHE_VALIDITY"},
				},
			},
			Action: func(ctx *cli.Context) error {
				opts := []server.Opt{
					server.WithAddress(address),
					server.WithPprofEndpoint("/debug/"),
				}
				if debug {
					opts = append(opts, server.WithLogrusLogWriter(logrus.DebugLevel))
				}

				s, err := server.NewServer(opts...)
				if err != nil {
					return fmt.Errorf("could not get a new server: %w", err)
				}

				config, err := rest.InClusterConfig()
				if err != nil {
					return fmt.Errorf("could not create in-cluster config: %w", err)
				}
				cliSet, err := kubernetes.NewForConfig(config)
				if err != nil {
					return fmt.Errorf("could not create new clientset: %w", err)
				}

				s.Router.HandleFunc(hcPath, func(w http.ResponseWriter, r *http.Request) {
					err := r.ParseForm()
					if err != nil {
						sendError(w, fmt.Errorf("could not parse form: %w", err), http.StatusBadRequest, s.Logger)
						return
					}
					_, ok := r.Form["svc"]
					if !ok {
						sendError(w, fmt.Errorf("query parameter svc is missing"), http.StatusBadRequest, s.Logger)
						return
					}
					serviceName, err := namespacedNameFromSVC(r.FormValue("svc"))
					if err != nil {
						sendError(w, fmt.Errorf("could not get namespaced service name from request query parameter svc: %w", err), http.StatusBadGateway, s.Logger)
					}

					if status, err := checkServiceHealth(ctx.Context, serviceName, *cliSet, cacheValidity, s.Logger); err != nil {
						sendError(w, fmt.Errorf("health check failed: %w", err), status, s.Logger)
						return
					}
				})

				return s.Run(ctx.Context)
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func namespacedNameFromSVC(svc string) (types.NamespacedName, error) {
	parts := strings.Split(svc, "/")
	switch len(parts) {
	case 2:
		return types.NamespacedName{
			Namespace: parts[0],
			Name:      parts[1],
		}, nil
	default:
		return types.NamespacedName{},
			fmt.Errorf("expected svc %s to have 2 parts after split with separator /, but got %d", svc, len(parts))
	}

}

func sendError(w http.ResponseWriter, err error, status int, logger server.Logger) {
	logger.Errorf("%v", err)
	var exp interface{}
	if err, ok := err.(interface{ ExpandedMsg() interface{} }); ok {
		exp = err.ExpandedMsg()
	}
	var msg = struct {
		Status      string      `json:"status,omitempty"`
		StatusCode  int         `json:"status_code,omitempty"`
		Msg         string      `json:"msg,omitempty"`
		ExpandedMsg interface{} `json:"expanded_msg,omitempty"`
	}{
		Status:      http.StatusText(status),
		StatusCode:  status,
		Msg:         err.Error(),
		ExpandedMsg: exp,
	}
	w.Header().Set("Content-type", "application/json")
	if status == http.StatusTooManyRequests {
		w.Header().Set("Retry-After", "30")
	}
	w.WriteHeader(status)
	if err := json.NewEncoder(w).Encode(msg); err != nil {
		logrus.Errorf("could not send error message: %v", err)
	}
}
