# cluster-health-checker

## Install latest version in cluster

To install the `cluster-health-checker` in a k8s cluster first make sure you are
using the correct k8s context with

```bash
kubectl config use-context <your cluster context>
```

then install it with

```bash
make install
```

This is going to install the latest version of the `cluster-health-checker` (see
`files/cluster-health-checker-pod.yaml` to see all the resources that are being
install on the cluster) in the `mt-operator-system` namespace. Thus this
namespace must be created if it does not exist already.

Use

```bash
make uninstall
```

to uninstall the `cluster-health-checker`.

## How to use it

By default the `cluster-health-checker` is listening for requests on port `8080`.

To locally check whether the `cluster-health-checker` is working as expected, use

```bash
kubectl port-forward -n mt-operator-system health-checker-pod 8080
```

or

```bash
kubectl port-forward -n mt-operator-system services/mt-operator-health-checker-svc 8080
```

(to connect via the health checker service).

Use the endpoint `/healthcheck` to check whether a service in the cluster is
healthy. The service is considered to be healthy if and only if there is at
least one [k8s
endpoint](https://kubernetes.io/docs/concepts/services-networking/service/#endpoints)
(with the same name as the service) which is ready.

Which service you want to check can be defined in the request url via a parameter:

```bash
curl -v http://localhost:8080/healthcheck?svc=<namespace>/<service name>
```

E.g.

```bash
curl -v http://localhost:8080/healthcheck?svc=pruning-molly/pruning-molly-magnolia-helm-public-svc
```

would do the healthcheck for the service
`pruning-molly-magnolia-helm-public-svc` in namespace `pruning-molly`.

> Note: The endpoint does not have to be `/healthcheck`. That is only a default.
> One can configure this path by using `-p <endpoint path>` when running the
> healthchecker or by defining environment variable
> `CLUSTER_HEALTH_CHECKER_PATH`.
