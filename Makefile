IMAGE_NAME=gitlab.com/mironet/cluster-health-checker
TARGET=cluster-health-checker
DOCKER_FILE=docker/Dockerfile

# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## build the test program
	go build -o $(TARGET)

build-docker: export DOCKER_BUILDKIT = 1
build-docker: ## Build the Docker image.
	docker build --build-arg APP_VERSION=$$(git describe --always) -t $(IMAGE_NAME) -f $(DOCKER_FILE) .

test:
	go fmt $(go list ./... | grep -v /vendor/)
	go vet $(go list ./... | grep -v /vendor/)
	go test -race $(go list ./... | grep -v /vendor/)

install:
	kubectl apply -f files/cluster-health-checker-pod.yaml 

uninstall:
	kubectl delete -f files/cluster-health-checker-pod.yaml 