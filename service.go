package main

import (
	"context"
	"fmt"
	"net/http"
	"sync"
	"time"

	"gitlab.com/mironet/server"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes"
)

type numReadyCacheElement struct {
	numReadyEndpoints int
	validUntil        time.Time
}

var numReadyCache = make(map[string]numReadyCacheElement)
var cacheLock sync.Mutex

func checkServiceHealth(ctx context.Context, serviceName types.NamespacedName, cliSet kubernetes.Clientset,
	cacheValidity time.Duration, log server.Logger) (int, error) {
	// Try to answer using only the cache.
	fullServiceName := serviceName.String()

	cacheLock.Lock()
	elem, ok := numReadyCache[fullServiceName]
	cacheLock.Unlock()
	if ok && !elem.validUntil.IsZero() {
		switch {
		// If the cached entry is still valid.
		case time.Now().Before(elem.validUntil):
			// Return it.
			log.Debugf(
				"answering health check request for service %s from cache (still valid for %s)",
				fullServiceName, time.Until(elem.validUntil),
			)
			return isOK(elem.numReadyEndpoints)
		// If the cached entry is no longer valid.
		default:
			//Delete it.
			cacheLock.Lock()
			delete(numReadyCache, fullServiceName)
			cacheLock.Unlock()
		}
	}

	// Get the endpoints (they have the same name and namespace as the service).
	endpoints, err := cliSet.CoreV1().Endpoints(serviceName.Namespace).Get(ctx, serviceName.Name, v1.GetOptions{})
	if err != nil {
		return http.StatusNotFound, err
	}

	// Count the number of ready endpoints.
	numReadyEndpoints := 0
	for _, subset := range endpoints.Subsets {
		numReadyEndpoints = numReadyEndpoints + len(subset.Addresses)
	}

	// Write the result into the cache.
	cacheLock.Lock()
	numReadyCache[fullServiceName] = numReadyCacheElement{
		numReadyEndpoints: numReadyEndpoints,
		validUntil:        time.Now().Add(cacheValidity),
	}
	cacheLock.Unlock()

	return isOK(numReadyEndpoints)
}

func isOK(numReadyEndpoints int) (int, error) {
	if numReadyEndpoints <= 0 {
		return http.StatusInternalServerError, fmt.Errorf("no endpoints ready")
	}
	return http.StatusOK, nil
}
